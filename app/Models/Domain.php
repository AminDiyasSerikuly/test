<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Domain extends Model
{
    use HasFactory;

    protected $fillable = [
        'organization_name',
        'organization_address',
        'postcode',
        'organization_phone',
        'organization_email',
        'organization_bin',
        'iik_kz',
        'bank_name',
        'bik',
        'organization_head_full_name',
        'responsible_person_full_name',
        'responsible_person_phone',
        'responsible_person_email',
        'domain',
        'certificate_state_registration'
    ];

    protected $table = 'domains';


    public static function store($request)
    {
        try {
            DB::beginTransaction();
            $file_path = self::fileUpload($request);

            $request->certificate_state_registration = $file_path;

            $newDomain = Domain::create($request->all());

            \Mail::to('amindiyass@gmail.com')->send(new \App\Mail\T_Mail($newDomain));
            DB::commit();
            return true;
        } catch (\Exception $exception) {
            DB::rollBack();
            return false;
        }

    }

    public static function fileUpload($request)
    {
        if ($request->file()) {
            $fileName = time() . '_' . $request->file('certificate_state_registration')->getClientOriginalName();
            $filePath = $request->file('certificate_state_registration')->storeAs('uploads', $fileName, 'public');

            $name = time() . '_' . $request->file('certificate_state_registration')->getClientOriginalName();
            return '/storage/' . $filePath;
        }
    }


}

