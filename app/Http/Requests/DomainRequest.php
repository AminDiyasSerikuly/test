<?php

namespace App\Http\Requests;

use App\Models\Domain;
use Illuminate\Foundation\Http\FormRequest;

class DomainRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'organization_name' => 'required',
            'organization_address' => 'required',
            'postcode' => 'required|integer',
            'organization_phone' => 'required|integer',
            'organization_email' => 'required|email',
            'organization_bin' => 'required',
            'iik_kz' => 'required',
            'bank_name' => 'required',
            'bik' => 'required|min:8',
            'organization_head_full_name' => 'required',
            'responsible_person_full_name' => 'required',
            'responsible_person_phone' => 'required|integer',
            'responsible_person_email' => 'required|email',
            'domain' => 'required',
//            'certificate_state_registration' => 'required|mimes:csv,txt,xlx,xls,pdf|max:2048',
            'captcha' => 'required|captcha',

        ];
    }

    public function validationData()
    {
        return $this->post();
    }

    public function messages()
    {
        return [
            'organization_name.required' => 'Необходимо заполнить «Название организации *».',
            'organization_address.required' => 'Необходимо заполнить «Юридический адрес *».',
            'postcode.required' => 'Необходимо заполнить «Эл. почтовый индекс*».',
            'organization_phone.required' => 'Необходимо заполнить «Телефон организации *».',
            'organization_email.required' => 'Необходимо заполнить «E-mail адрес организации *».',
            'organization_bin.required' => 'Необходимо заполнить «БИН организации (12 цифр) *».',
            'iik_kz.required' => 'Необходимо заполнить «БИН организации (12 цифр) *».',
            'bank_name.required' => 'Необходимо заполнить «Название банка *».',
            'bik.required' => 'Необходимо заполнить «БИК (8 латинских букв и цифр) *».',
            'organization_head_full_name.required' => 'Необходимо заполнить «ФИО (руководителя) *».',
            'responsible_person_full_name.required' => 'Необходимо заполнить «ФИО (ответственного лица) *».',
            'responsible_person_phone.required' => 'Необходимо заполнить «телефон (ответственного лица) *».',
            'responsible_person_email.required' => 'Необходимо заполнить «Е –mail (ответственного лица) *».',
            'domain.required' => 'Необходимо заполнить «желаемое название домена в зоне edu.kz, например: название организации.edu.kz *».',
            'captcha.captcha' => 'Неправильный проверочный код',
            'captcha.required' => 'Необходимо заполнить проверочный код',

        ];
    }


}
