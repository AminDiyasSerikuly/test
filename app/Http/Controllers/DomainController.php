<?php

namespace App\Http\Controllers;

use App\Http\Requests\DomainRequest;
use App\Models\Domain;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DomainController extends Controller
{
    public function index()
    {
        return view('domain.create');
    }


    public function store(DomainRequest $request)
    {

        if (Domain::store($request)) {
            return 'success';
        }
        return 'error';

    }

    public function refreshCaptcha()
    {
        return response()->json(['captcha' => captcha_img()]);
    }


}
