<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class T_Mail extends Mailable
{
    use Queueable, SerializesModels;


    public $domain;
    public function __construct($domain)
    {
        $domain = $this->domain;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('This is Testing Mail')
            ->view('email.test');
    }
}
