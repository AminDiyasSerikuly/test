<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
      integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">


<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>


<div id="main_container" class="container">
    <h1 id="main_title">
        Форма для регистрации домена в зоне EDU.KZ для юридических лиц
    </h1>


    <form action="{{route('domain.store')}}" method="POST" enctype="multipart/form-data"
          style="background-color: #F8F4F1; padding: 10px;">
        @csrf
        <div class="row">
            <div class="col-6 form-group">
                <label for="organization_name">Наименование организации (пропишите полностью) *</label>
                <input name="organization_name" class="form-control" id="organization_name">
                @error('organization_name')
                <span class="valid_show">
                    {{$message}}
                </span>
                @enderror
            </div>

            <div class="col-6 form-group">
                <label for="organization_address">Юридический адрес организации, включая город/область *</label>
                <input name="organization_address" class="form-control" id="organization_address">
                @error('organization_address')
                <span class="valid_show">
                    {{$message}}
                </span>
                @enderror
            </div>

            <div class="col-6 form-group">
                <label for="postcode">Почтовый индекс (6 цифр) *</label>
                <input type="number" name="postcode" class="form-control" id="postcode">
                @error('postcode')
                <span class="valid_show">
                    {{$message}}
                </span>
                @enderror
            </div>

            <div class="col-6 form-group">
                <label for="organization_phone">Телефон организации *</label>
                <input type="number" name="organization_phone" class="form-control" id="organization_phone">
                @error('organization_phone')
                <span class="valid_show">
                    {{$message}}
                </span>
                @enderror
            </div>


            <div class="col-6 form-group">
                <label for="organization_email">E-mail адрес организации *</label>
                <input type="email" name="organization_email" class="form-control" id="organization_email">
                @error('organization_email')
                <span class="valid_show">
                    {{$message}}
                </span>
                @enderror
            </div>

            <div class="col-6 form-group">
                <label for="organization_bin">БИН организации (12 цифр) *</label>
                <input type="number" name="organization_bin" class="form-control" id="organization_bin">
                @error('organization_bin')
                <span class="valid_show">
                    {{$message}}
                </span>
                @enderror
            </div>

            <div class="col-6 form-group">
                <label for="iik_kz">ИИК KZ (20 значный счет) *</label>
                <input name="iik_kz" class="form-control" id="iik_kz">
                @error('iik_kz')
                <span class="valid_show">
                    {{$message}}
                </span>
                @enderror
            </div>

            <div class="col-6 form-group">
                <label for="bank_name">Наименование банка *</label>
                <input name="bank_name" class="form-control" id="bank_name">
                @error('bank_name')
                <span class="valid_show">
                    {{$message}}
                </span>
                @enderror
            </div>

            <div class="col-6 form-group">
                <label for="bik">БИК (8 латинских букв и цифр) *</label>
                <input name="bik" class="form-control" id="bik">
                @error('bik')
                <span class="valid_show">
                    {{$message}}
                </span>
                @enderror
            </div>

            <div class="col-6 form-group">
                <label for="organization_head_full_name">ФИО руководителя организации *</label>
                <input name="organization_head_full_name" class="form-control" id="organization_head_full_name">
                @error('organization_head_full_name')
                <span class="valid_show">
                    {{$message}}
                </span>
                @enderror
            </div>

            <div class="col-6 form-group">
                <label for="responsible_person_full_name">Ответственное лицо *</label>
                <input name="responsible_person_full_name" class="form-control" id="responsible_person_full_name">
                @error('responsible_person_full_name')
                <span class="valid_show">
                    {{$message}}
                </span>
                @enderror
            </div>

            <div class="col-6 form-group">
                <label for="responsible_person_phone">Телефон ответственного лица *</label>
                <input name="responsible_person_phone" class="form-control" id="responsible_person_phone">
                @error('responsible_person_phone')
                <span class="valid_show">
                    {{$message}}
                </span>
                @enderror
            </div>

            <div class="col-6 form-group">
                <label for="responsible_person_email">E-mail ответственного лица *</label>
                <input  name="responsible_person_email" class="form-control" id="responsible_person_email">
                @error('responsible_person_email')
                <span class="valid_show">
                    {{$message}}
                </span>
                @enderror
            </div>

            <div class="col-6 form-group">
                <label for="domain">Желаемое название доменного имени в зоне edu.kz *</label>
                <input name="domain" class="form-control" id="domain">
                @error('domain')
                <span class="valid_show">
                    {{$message}}
                </span>
                @enderror
            </div>


            <div class="col-6 form-group" style="padding-top: 30px;">
                <label
                    style=" cursor: pointer;border-radius: 3px; color: white;background-color: #0392D6; padding: 20px;font-size: 15px;"
                    for="certificate_state_registration">
                    Загрузите копию свидетельства о государственной регистрации организации
                    или другой документ удостоверяющий организацию в pdf (Размер не должен превышать 2.00 МБ)
                </label>
                <input type="file" name="certificate_state_registration" class="form-control-file" id="">
                @error('certificate_state_registration')
                <span class="valid_show">
                    {{$message}}
                </span>
                @enderror
            </div>

            <div class="col-6" style="padding-top: 30px;">
                <div class="captcha">
                    <span class="btn-refresh">{!! captcha_img() !!}</span>
                </div>
                <br>
                <input id="captcha" type="text" class="form-control" placeholder="Введите код с картинки*" name="captcha">


                @if ($errors->has('captcha'))
                    <span class="valid_show">
                                  <strong>{{ $errors->first('captcha') }}</strong>
                              </span>
                @endif
            </div>

        </div>


        <button type="submit" class="btn btn-success">Отправить</button>

    </form>
</div>


<script type="text/javascript">


    $(".btn-refresh").click(function () {
        $.ajax({
            type: 'GET',
            url: '/refresh_captcha',
            success: function (data) {
                $(".captcha span").html(data.captcha);
            }
        });
    });


</script>

<style>
    #main_title {
        margin-bottom: 26px;
        font-size: 55px;
        line-height: 60px;
        font-family: 'Arial'
    }

    #main_container {
        padding: 60px;
    }

    .valid_show {
        color: red;
        font-size: 80%;
    }
</style>
