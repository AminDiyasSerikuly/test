<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDomainsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('domains', function (Blueprint $table) {
            $table->id();
            $table->string('organization_name')->comment('Наименование организации');
            $table->string('organization_address')->comment('Юридический адрес организации');
            $table->integer('postcode')->comment('Почтовый индекс');
            $table->integer('organization_phone')->comment('Телефон организации');
            $table->string('organization_email')->comment('E-mail адрес организации');
            $table->integer('organization_bin')->comment('БИН организации');
            $table->integer('iik_kz')->comment('ИИК KZ ');
            $table->string('bank_name')->comment('Наименование банка');
            $table->string('bik')->comment('БИК');
            $table->string('organization_head_full_name')->comment('ФИО руководителя организации');
            $table->string('responsible_person_full_name')->comment('Ответственное лицо');
            $table->integer('responsible_person_phone')->comment('Телефон ответственного лица');
            $table->string('responsible_person_email')->comment('E-mail ответственного лица');
            $table->string('domain')->comment('Желаемое название доменного имени в зоне edu.kz');
            $table->string('certificate_state_registration')->comment('Свидетельство о государственной регистрации организации');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('domains');
    }
}
