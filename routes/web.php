<?php

use App\Http\Controllers\DomainController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('domain.create');
});


Route::post('domain/store', [DomainController::class, 'store'])->name('domain.store');
Route::get('refresh_captcha', [DomainController::class, 'refreshCaptcha'])->name('refresh_captcha');

